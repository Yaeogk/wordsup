﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class shoplistreload : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {/*
        Screen.SetResolution(768, 1024, false);
        Screen.fullScreen = false;
        */
        List<string> list;
        string path = Path.Combine(Application.persistentDataPath, "list");
        if (System.IO.File.Exists(path))
        {
            list = Read();
        }
        else
        {
            PlayerPrefs.SetInt("coins", 100);
            list = new List<string>(Readfirstime());
        }
        


        StreamWriter sw = new StreamWriter(path, false);
        for (int i = 0; i < list.Count; i++)
        {
            sw.WriteLine(list[i]);
        }
        sw.Close();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    string[] Readfirstime()
    {
        TextAsset textFile = (TextAsset)Resources.Load("Shop/List", typeof(TextAsset));
        string content = textFile.text;
        string[] lines = content.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        return lines;
    }

    List<string> Read()
    {
        string path = Path.Combine(Application.persistentDataPath, "list");
        StreamReader sr = new StreamReader(path);
        string line;
        List<string> lines = new List<string>();
        int i = 0;
        while ((line = sr.ReadLine()) != null)
        {
            lines.Add(line);
            i++;
        }
        sr.Close();
        return lines;
    }
}
