﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class CollectionGeneratorController : MonoBehaviour
{
    public GameObject Item;
    public float offset;
    public Canvas Canvas;
    GameObject i;


    struct ItemSize
    {
        public float x;
        public float y;
    };



    // Start is called before the first frame update
    void Start()
    {
        RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();
        ItemSize iz;
        //get param
        i = Instantiate(Item, new Vector3(0, 0, 0), Quaternion.identity);
        Destroy(i);
        iz.x = i.GetComponent<SpriteRenderer>().bounds.size.x;
        iz.y = i.GetComponent<SpriteRenderer>().bounds.size.y;
        float screenW = Camera.main.orthographicSize * 2 * Camera.main.aspect;
        int maxX = (int)Math.Floor((screenW - offset) / (iz.x + offset));

        List<string> list = Read();

        int c = -1;
        int d = 0;
        string sub;
        foreach (string x in list)
        {
            sub = x.Substring(x.IndexOf("|") + 1);
            if (sub.EndsWith("+") == true)
            {
                if (c < 1) c++;
                else
                {
                    d++;
                    c = 0;
                }

                i = Instantiate(Item, new Vector3(-1.1f + ((c * (iz.x + offset))), 3f - (d * (iz.y + offset)), -1), Quaternion.identity);
                i.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Shop/" + sub);
                i.transform.SetParent(transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    List<string> Read()
    {
        string path = Path.Combine(Application.persistentDataPath, "list");
        StreamReader sr = new StreamReader(path);       
        string line;        
        List<string> lines = new List<string>();
        int i = 0;
        while ((line = sr.ReadLine()) != null)
        {
            lines.Add(line);
            i++;
        }
        sr.Close();
        return lines;
    }
}
