﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyCoinController : MonoBehaviour
{
    public Canvas Canvas;
    private GameObject Box;
    private GameObject DropDown;
    private GameObject Okay;
    private GameObject Cancel;
    public void Buy()
    {
        GameObject Box = Resources.Load<GameObject>("Prefabs/BuyCoinBox");
        GameObject DropDown = Resources.Load<GameObject>("Prefabs/Dropdown");
        GameObject Okay = Resources.Load<GameObject>("Prefabs/BuyCoinOk");
        GameObject Cancel = Resources.Load<GameObject>("Prefabs/BuyCoinCancel");

        DisEn(false);

        Box = Instantiate(Box, Box.transform.position, Box.transform.rotation);
        Box.transform.SetParent(Canvas.transform, false);      
        DropDown = Instantiate(DropDown, DropDown.transform.position, DropDown.transform.rotation);
        DropDown.transform.SetParent(Canvas.transform, false);
        Okay = Instantiate(Okay, Okay.transform.position, Okay.transform.rotation);
        Okay.transform.SetParent(Canvas.transform, false);
        Cancel = Instantiate(Cancel, Cancel.transform.position, Cancel.transform.rotation);
        Cancel.transform.SetParent(Canvas.transform, false);

    }
    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Button>() != null) par.GetChild(i).gameObject.GetComponent<Button>().enabled = b;
        }

        par = transform.parent.gameObject.transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }
}
