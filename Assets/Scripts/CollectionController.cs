﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectionController : MonoBehaviour
{
    public void Collection()
    {
        if (SceneManager.GetSceneByName("Collection").isLoaded == true)
        {
            SceneManager.UnloadSceneAsync("Collection");
            SceneManager.LoadScene("Collection", LoadSceneMode.Additive);
        }
        else SceneManager.LoadScene("Collection", LoadSceneMode.Additive);

        if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);
        if (GameObject.FindWithTag("MenuScene") != null) deactivate(GameObject.FindWithTag("MenuScene").transform, false);
        if (GameObject.FindWithTag("ShopScene") != null) deactivate(GameObject.FindWithTag("ShopScene").transform, false);

        //else
        //{
        // if (GameObject.FindWithTag("CollectionScene") != null) deactivate(GameObject.FindWithTag("CollectionScene").transform, true);
        // if (GameObject.FindWithTag("ShopScene") != null) deactivate(GameObject.FindWithTag("ShopScene").transform, false);
        // if (GameObject.FindWithTag("MenuScene") != null) deactivate(GameObject.FindWithTag("MenuScene").transform, false);
        // if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);          
        //}

    }

    public void deactivate(Transform x, bool b)
    {
        int childCnt = x.childCount;
        for (int i = 0; i < childCnt; i++)
        {
            x.GetChild(i).gameObject.SetActive(b);
        }
    }
}