﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LetterController : MonoBehaviour
{
    private char type;
    private bool state;
    private GameObject obj;
    private GameObject objc;

    private Vector3 screenPoint;
    private Vector3 offset;
    private bool frameflag;
    private bool letterflag;
    private Vector3 framepos;
    private Vector3 boardpos;


    //------ set get ------
    public void setType(char x)
    {
        type = x;
    }
    public char getType()
    {
        return type;
    }

    public void setState(bool x)
    {
        state = x;
    }
    public bool getState()
    {
        return state;
    }


    public void loadSprite()
    {
        this.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Letters/" + type.ToString());
    }

    public Vector3 getboardpos()
    {
        return boardpos;
    }

    // Start is called before the first frame update
    void Start()
    {
        letterflag = false;
        frameflag = false;
        state = false;
        boardpos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }
   
    private void OnTriggerStay2D(Collider2D other)
    {      
        if (!gameObject.GetComponent<LetterController>().enabled)
        {
            return;
        }
        if (other.transform.tag == "Frame")
        {
            frameflag = true;
            framepos = new Vector3(other.gameObject.transform.position.x, other.gameObject.transform.position.y, other.gameObject.transform.position.z - 1);
            obj = other.gameObject;      
        }

        if (other.transform.tag == "Letter")
        {
            letterflag = true;
            objc = other.gameObject;
        }

    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!gameObject.GetComponent<LetterController>().enabled)
        {
            return;
        }

        if (other.transform.tag == "Frame")
        {
            frameflag = false;
        }

        if (other.transform.tag == "Letter")
        {
            letterflag = false;
        }
    }

    void OnMouseUp()
    {
        if (!gameObject.GetComponent<LetterController>().enabled)
        {
            return;
        }

        if (state == true)
        {
            return;
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
        if ((frameflag == true))
        {
            if (letterflag == false)
            {
                this.transform.position = framepos;
                if (obj.GetComponent<FrameController>().getType() == this.getType()) obj.GetComponent<FrameController>().setState(true);
                obj.GetComponent<FrameController>().setobj(gameObject);
            }
            else
            {
                this.transform.position = boardpos;
                InvalidMove(framepos);
            }
        }

    }

    void OnMouseDown()
    {
        if (!gameObject.GetComponent<LetterController>().enabled)
        {
            return;
        }

        if (state == true)
        {
            return;
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
        screenPoint = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    void OnMouseDrag()
    {
        if (!gameObject.GetComponent<LetterController>().enabled)
        {
            return;
        }
        if (state == true)
        {
            return;
        }
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    private void InvalidMove(Vector3 pos)
    {
        GameObject Box;
        GameObject Okay;
        Canvas Canvas;
        //Vector2 posfix;
        //Vector2 viewportPoint;

        Box = Resources.Load<GameObject>("Prefabs/ErrorBox");
        Okay = Resources.Load<GameObject>("Prefabs/ErrorOkay");
        Canvas = FindObjectOfType<Canvas>();

        Box = Instantiate(Box, Box.transform.position, Box.transform.rotation);
        Box.transform.SetParent(Canvas.transform, false);
        Okay = Instantiate(Okay, Okay.transform.position, Okay.transform.rotation);
        Okay.transform.SetParent(Canvas.transform, false);

        Okay.GetComponent<ErrorOkayController>().setcolor(objc.GetComponent<SpriteRenderer>().color);
        Okay.GetComponent<ErrorOkayController>().setobj(objc);
        objc.GetComponent<SpriteRenderer>().color = new Color(1f, 0.5f, 0.5f, 1f);

        DisEn(false);

        //positioning
        /*
        Box = Instantiate(Box, pos, Box.transform.rotation);
        pos= new Vector3(pos.x-0.67f, pos.y+0.42f, pos.z - 1);
        viewportPoint = Camera.main.WorldToViewportPoint(pos);

        Box.GetComponent<RectTransform>().anchorMin = viewportPoint;
        Box.GetComponent<RectTransform>().anchorMax = viewportPoint;
        Box.transform.position = pos;
        Box.transform.SetParent(Canvas.transform, false);

        Okay = Instantiate(Okay, pos, Okay.transform.rotation);
        pos = new Vector3(pos.x, pos.y -0.12f, pos.z);
        viewportPoint = Camera.main.WorldToViewportPoint(pos);

        Okay.GetComponent<RectTransform>().anchorMin = viewportPoint;
        Okay.GetComponent<RectTransform>().anchorMax = viewportPoint;
        Okay.transform.position = pos;
        Okay.transform.SetParent(Canvas.transform, false);  
        */


    }
    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Canvas>() != null)
            {
                for (int j = 0; j < par.GetChild(i).childCount; j++)
                {
                    if (par.GetChild(i).GetChild(j).gameObject.GetComponent<Canvas>() != null)
                    {
                        par.GetChild(i).GetChild(j).gameObject.GetComponent<Button>().enabled = b;
                    }
                }
            }
        }
        par = transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }

}

