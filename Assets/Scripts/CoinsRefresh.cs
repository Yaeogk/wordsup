﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsRefresh : MonoBehaviour
{
    public UnityEngine.UI.Text text;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        text.text="$"+PlayerPrefs.GetInt("coins").ToString();
    }
}
