﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RulesOkayController : MonoBehaviour
{

    public void Okay()
    {
        GameObject x = transform.parent.GetChild(transform.parent.childCount - 2).gameObject;
        x.transform.parent = null;
        Destroy(x);

        DisEn(true);

        Destroy(gameObject);
    }

    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Button>() != null) par.GetChild(i).gameObject.GetComponent<Button>().enabled = b;
        }

        par = transform.parent.gameObject.transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }
}
