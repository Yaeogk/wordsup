﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopController : MonoBehaviour
{
    public void Shop()
    {
        if (SceneManager.GetSceneByName("Shop").isLoaded == false)
        {
            SceneManager.LoadScene("Shop", LoadSceneMode.Additive);
            if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);
            if (GameObject.FindWithTag("MenuScene") != null) deactivate(GameObject.FindWithTag("MenuScene").transform, false);
            if (GameObject.FindWithTag("CollectionScene") != null) deactivate(GameObject.FindWithTag("CollectionScene").transform, false);
        }
        else
        {
            if (GameObject.FindWithTag("ShopScene") != null) deactivate(GameObject.FindWithTag("ShopScene").transform, true);
            if (GameObject.FindWithTag("MenuScene") != null) deactivate(GameObject.FindWithTag("MenuScene").transform, false);
            if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);          
            if (GameObject.FindWithTag("CollectionScene") != null) deactivate(GameObject.FindWithTag("CollectionScene").transform, false);
        }

    }

    public void deactivate(Transform x, bool b)
    {
        int childCnt = x.childCount;
        for (int i = 0; i < childCnt; i++)
        {
            x.GetChild(i).gameObject.SetActive(b);
        }
    }
}
