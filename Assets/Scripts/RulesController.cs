﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class RulesController : MonoBehaviour
{
    private GameObject Box;
    private GameObject Okay;   
    public Canvas Canvas;

    public void Rules()
    {
        Box = Resources.Load<GameObject>("Prefabs/RulesBox");
        Okay = Resources.Load<GameObject>("Prefabs/RulesOkay");

        DisEn(false);


        Box = Instantiate(Box, Box.transform.position, Box.transform.rotation);
        Box.transform.SetParent(Canvas.transform, false);
        Okay = Instantiate(Okay, Okay.transform.position, Okay.transform.rotation);
        Okay.transform.SetParent(Canvas.transform, false);


    }

    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Button>() != null) par.GetChild(i).gameObject.GetComponent<Button>().enabled = b;
        }

        par = transform.parent.gameObject.transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }
}
