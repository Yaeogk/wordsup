﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;


public class LevelGeneratorController : MonoBehaviour
{
    //Grid grid;
    public GameObject Frame;
    public GameObject Letter;
    //public GameObject GameObjectManager;

    private List<GameObject> Frames = new List<GameObject>();

    public Camera cam;
    public float offset;
    public int levelcount;
    private GameObject f, l;
    

    struct FrameSize
    {
        public float x;
        public float y;
    };

    struct LetterSize
    {
        public float x;
        public float y;
    };


    // Start is called before the first frame update
    void Start()
    {
        //List<GameObject> Frames = new List<GameObject>();
        Generate();
        

    }

    // Update is called once per frame
    void Update()
    {
        int c = 0;
        int cc = 0;
        foreach (GameObject x in Frames)
        {
            if (x.GetComponent<FrameController>().getState() == true) c++;  
            if (x.GetComponent<FrameController>().getIsOccupied() == true) cc++;
        }
        if (c == Frames.Count)
        {
            //PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins")+1000);
            SceneManager.LoadScene("Game", LoadSceneMode.Single);
        }
        if (cc == Frames.Count) ;
            //pranesimas
    }

    /*
    string[] ReadW()
    {
        TextAsset textFile = (TextAsset)Resources.Load("words", typeof(TextAsset));
        string content = textFile.text;
        string[] lines = content.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        return lines;
    }
    */

    string[] Read()
    {
        System.Random rnd = new System.Random();
        int randomInt = rnd.Next(1, levelcount + 1);
        TextAsset textFile = (TextAsset)Resources.Load("Levels/" + randomInt.ToString(), typeof(TextAsset));
        string content = textFile.text;
        string[] lines = content.Split(new string[] { "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
        return lines;
    }

    void Generate()
    {
        List<string> lines = new List<string>(Read());
        System.Random rnd = new System.Random();      
        FrameSize fs;
        LetterSize ls;
        int maxX;
        //int maxY;
        float screenW;

        int randomInt = 0;
        //string wordToFrame;


        //--------get param
        f = Instantiate(Frame, new Vector3(0, 0, 0), Quaternion.identity);
        Destroy(f);
        fs.x = f.GetComponent<SpriteRenderer>().bounds.size.x;
        fs.y = f.GetComponent<SpriteRenderer>().bounds.size.y;
        screenW = Camera.main.orthographicSize * 2 * Camera.main.aspect;
        maxX = (int)Math.Floor((screenW - offset) / (fs.x + offset));
        //maxY = (int)Math.Floor(cam.rect.height / (fs.y + (2 * offset)));

        char[,] Grid = new char[9, 9];
        List<char> letters = new List<char>();

        for (int x = 0; x < 9; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                Grid[x, y] = lines[x][y];
            }
        }
        var topLeft = (Vector2)Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, Camera.main.nearClipPlane));
        var bottomLeft = (Vector2)Camera.main.ScreenToWorldPoint(new Vector3(0, 0 / 8f, Camera.main.nearClipPlane));

        for (int x = 0; x < 9; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                if (Grid[x, y] != '0')
                {
                    f = Instantiate(Frame, new Vector3(topLeft.x + (((y + 1) * (fs.y + offset))), topLeft.y - (((x + 2) * (fs.x + offset))), -1), Quaternion.identity);
                    Frames.Add(f);
                    f.GetComponent<FrameController>().setType(Grid[x, y]);
                    letters.Add(Grid[x, y]);
                    f.transform.SetParent(transform.parent.transform);
                }             
            }
        }


        

        //--------get param
        l = Instantiate(Frame, new Vector3(0, 0, 0), Quaternion.identity);
        Destroy(l);
        ls.x = l.GetComponent<SpriteRenderer>().bounds.size.x;
        ls.y = l.GetComponent<SpriteRenderer>().bounds.size.y;


        int c = -1;
        int d = 0;
        while (letters.Count>0)
        {
            if (c < 7) c++;
            else
            {
                d++;
                c = 0;
            }



            randomInt = rnd.Next(0, letters.Count);
            l = Instantiate(Letter, new Vector3(-2.5f + ((c * (ls.x + offset))), -2.2f - ((d * (ls.y + offset))), -2), Quaternion.identity);
            l.GetComponent<LetterController>().setType(letters[randomInt]);          
            l.GetComponent<LetterController>().loadSprite();
            letters.RemoveAt(randomInt);
            l.transform.SetParent(transform.parent.transform);
        }
    }

}

