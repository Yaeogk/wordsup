﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameController : MonoBehaviour
{
    private GameObject obj;
    private char type;
    private bool isOccupied = false;
    private bool state = false; // currently connected to the right piece?
    private Rect size;

    private int colcount = 0;

    //------ set get ------
    public void setType(char x)
    {
        type = x;
    }
    public char getType()
    {
        return type;
    }

    public void setIsOccupied(bool x)
    {
        isOccupied = x;
    }
    public bool getIsOccupied()
    {
        return isOccupied;
    }

    public void setState(bool x)
    {
        state = x;
    }
    public bool getState()
    {
        return state;
    }

    public void setobj(GameObject x)
    {
        obj = x;
    }
    public GameObject getobj()
    {
        return obj;
    }

    //--------------------


    // Start is called before the first frame update
    void Start()
    {
        //isOccupied=false;
        //state = false;
        //isConnector = false;
        //isHorizontal = true;
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!gameObject.GetComponent<FrameController>().enabled)
        {
            return;
        }

        if (other.transform.tag == "Letter")
        {
            colcount++;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (!gameObject.GetComponent<FrameController>().enabled)
        {
            return;
        }

        if (other.transform.tag == "Letter")
        {
            colcount--;
            if (colcount == 0)
            {
                state = false;
                obj = null;
            }
        }

    }

}
