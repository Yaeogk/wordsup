﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class BuyItemController : MonoBehaviour
{
    private int price;
    public void Setprice(int x)
    {
        price = x;
    }
    public int Getprice()
    {
        return price;
    }
    public void Buy()
    {
        if (PlayerPrefs.GetInt("coins") < price) {; }//
        else
        {
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - price); 
        List<string> list = Read();
        string x = transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite.name;

        string path = Path.Combine(Application.persistentDataPath, "list");
        StreamWriter sw = new StreamWriter(path, false);
        for (int i=0; i<list.Count;i++ )
        {          
            if (list[i].Substring(list[i].IndexOf("|")+1) == x)
            {
                list[i] += "+";
            }
            sw.WriteLine(list[i]);
        }
        sw.Close();

        x += "+";
        transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Shop/" + x);
        transform.GetChild(1).gameObject.transform.SetParent(transform.parent);

        Destroy(gameObject);
        }
    }

    List<string> Read()
    {
        string path = Path.Combine(Application.persistentDataPath, "list");
        StreamReader sr = new StreamReader(path);
        string line;
        List<string> lines = new List<string>();
        int i = 0;
        while ((line = sr.ReadLine()) != null)
        {
            lines.Add(line);
            i++;
        }
        sr.Close();

        return lines;
    }
}

