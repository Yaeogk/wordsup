﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackController : MonoBehaviour
{
    public void Back()
    {
        if (SceneManager.GetSceneByName("Menu").isLoaded == false)
        {            
            SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
            if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);
            if (GameObject.FindWithTag("ShopScene") != null) deactivate(GameObject.FindWithTag("ShopScene").transform, false);
            if (GameObject.FindWithTag("CollectionScene") != null) deactivate(GameObject.FindWithTag("CollectionScene").transform, false);
        }
        else
        {
            if (GameObject.FindWithTag("MenuScene")!= null) deactivate(GameObject.FindWithTag("MenuScene").transform, true);
            if (GameObject.FindWithTag("GameScene") != null) deactivate(GameObject.FindWithTag("GameScene").transform, false);
            if (GameObject.FindWithTag("ShopScene") != null) deactivate(GameObject.FindWithTag("ShopScene").transform, false);
            if (GameObject.FindWithTag("CollectionScene") != null) deactivate(GameObject.FindWithTag("CollectionScene").transform, false);
        }

    }

    public void deactivate(Transform x, bool b)
    {
        int childCnt = x.childCount;
        for (int i = 0; i < childCnt; i++)
        {
            x.GetChild(i).gameObject.SetActive(b);
        }
    }
}
