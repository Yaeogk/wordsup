﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayController : MonoBehaviour
{

    public void Play()
    {
        if (SceneManager.GetSceneByName("Game").isLoaded == false)
        {           
            SceneManager.LoadScene("Game", LoadSceneMode.Additive);
            deactivate(GameObject.FindWithTag("MenuScene").transform, false);
        }
        else
        {
            deactivate(GameObject.FindWithTag("MenuScene").transform, false);
            deactivate(GameObject.FindWithTag("GameScene").transform, true);
        }       
    }

    public void deactivate(Transform x, bool b)
    {
        int childCnt = x.childCount;
        for (int i = 0; i < childCnt; i++)
        {
            x.GetChild(i).gameObject.SetActive(b);
        }
    }
}
