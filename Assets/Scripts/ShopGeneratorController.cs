﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class ShopGeneratorController : MonoBehaviour
{
    public GameObject Item;
    public float offset;
    public GameObject BuyItem;
    public Canvas Canvas;
    GameObject i, b;


    struct ItemSize
    {
        public float x;
        public float y;
    };



    // Start is called before the first frame update
    void Start()
    {
        RectTransform CanvasRect = Canvas.GetComponent<RectTransform>();
        ItemSize iz;
        //get param
        i = Instantiate(Item, new Vector3(0, 0, 0), Quaternion.identity);
        Destroy(i);
        iz.x = i.GetComponent<SpriteRenderer>().bounds.size.x;
        iz.y = i.GetComponent<SpriteRenderer>().bounds.size.y;
        float screenW = Camera.main.orthographicSize * 2 * Camera.main.aspect;
        int maxX = (int)Math.Floor((screenW - offset) / (iz.x + offset));

        List<string> list = Read();

        Vector2 pos;
        Vector2 viewportPoint;

        int c = -1;
        int d = 0;
        string sub;
        foreach (string x in list)
        {
            if (c < 1) c++;
            else
            {
                d++;
                c = 0;
            }

            
            i = Instantiate(Item, new Vector3(-1.1f + ((c * (iz.x + offset))), 3f - (d * (iz.y + offset)), -1), Quaternion.identity);
            pos = new Vector3(-0.8f + ((c * (iz.x + offset))), 2.4f - ((d * (iz.y + offset))));

            sub = x.Substring(x.IndexOf("|")+1);
            if (sub.EndsWith("+") != true)
            {
                b = Instantiate(BuyItem, new Vector3(0, 0, 0), Quaternion.identity);
                viewportPoint = Camera.main.WorldToViewportPoint(pos);
                b.GetComponent<RectTransform>().anchorMin = viewportPoint;
                b.GetComponent<RectTransform>().anchorMax = viewportPoint;
                b.transform.position = new Vector3(b.transform.position.x, b.transform.position.y, -2);
                b.transform.SetParent(Canvas.transform, false);
                b.gameObject.GetComponent<BuyItemController>().Setprice(Int32.Parse(x.Substring(0, x.IndexOf("|"))));
                b.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = "Buy ($" + x.Substring(0, x.IndexOf("|")) + ")";
                i.transform.SetParent(b.transform);
            }
            else i.transform.SetParent(transform);

            i.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Shop/" + sub);           
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    List<string> Read()
    {

        string path = Path.Combine(Application.persistentDataPath, "list");
        StreamReader sr = new StreamReader(path);
        string line;
        List<string> lines = new List<string>();
        int i = 0;
        while ((line = sr.ReadLine()) != null)
        {
            lines.Add(line);
            i++;
        }
        sr.Close();
        return lines;
    }
}
