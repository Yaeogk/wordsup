﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ADcloseController : MonoBehaviour
{
    private bool flag=false;
    public int countdownTime;
    public Text countdownDisplay;
    private void Start()
    {
        StartCoroutine(CoundownToStart());
    }
    public void CloseAD()
    {
        if (flag == true)
        {
            this.transform.parent.GetChild(0).gameObject.GetComponent<VideoPlayer>().Stop();
            Destroy(gameObject);
        }

    }

    IEnumerator CoundownToStart()
    {

        while (countdownTime > 0)
        {            
            countdownDisplay.text = countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }
        countdownDisplay.text = "Close";
        flag = true;
    }
}
