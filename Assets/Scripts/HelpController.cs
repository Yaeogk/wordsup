﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class HelpController : MonoBehaviour
{
    public int helpcost;
    private GameObject Box;
    private GameObject Okay;
    public Canvas Canvas;

    public void Help()
    {
        if (PlayerPrefs.GetInt("coins") < helpcost)
        {
            Box = Resources.Load<GameObject>("Prefabs/HelpBox");
            Okay = Resources.Load<GameObject>("Prefabs/HelpOkay");

            DisEn(false);


            Box = Instantiate(Box, Box.transform.position, Box.transform.rotation);
            Box.transform.SetParent(Canvas.transform, false);
            Okay = Instantiate(Okay, Okay.transform.position, Okay.transform.rotation);
            Okay.transform.SetParent(Canvas.transform, false);
        }

        else
        {
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - helpcost);
            char type;
            Transform par = transform.parent.transform.parent;
            int childCnt = par.childCount;
            for (int i = 0; i < childCnt; i++)
            {
                if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null)
                {
                    type = par.GetChild(i).gameObject.GetComponent<LetterController>().getType();
                    for (int j = 0; j < childCnt; j++)
                    {
                        if (par.GetChild(j).gameObject.GetComponent<FrameController>() != null)
                        {
                            if (par.GetChild(j).gameObject.GetComponent<FrameController>().getType() == type)
                            {
                                if (par.GetChild(j).gameObject.GetComponent<FrameController>().getState() == false)
                                {
                                    if (par.GetChild(i).gameObject.GetComponent<LetterController>().getState() == false)
                                    {

                                        if (par.GetChild(j).gameObject.GetComponent<FrameController>().getobj() != null) par.GetChild(j).gameObject.GetComponent<FrameController>().getobj().transform.position = par.GetChild(j).gameObject.GetComponent<FrameController>().getobj().GetComponent<LetterController>().getboardpos();
                                        par.GetChild(i).gameObject.GetComponent<SpriteRenderer>().color = new Color(0.62f, 1f, 0.49f, 1f);
                                        par.GetChild(i).gameObject.GetComponent<LetterController>().setState(true);
                                        par.GetChild(j).gameObject.GetComponent<FrameController>().setState(true);
                                        par.GetChild(i).gameObject.transform.position = new Vector3(par.GetChild(j).gameObject.transform.position.x, par.GetChild(j).gameObject.transform.position.y, -2);
                                        return;
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Button>() != null) par.GetChild(i).gameObject.GetComponent<Button>().enabled = b;
        }

        par = transform.parent.gameObject.transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }
}
