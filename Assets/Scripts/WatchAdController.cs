﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class WatchAdController : MonoBehaviour
{
    public RawImage rawimage;
    public RawImage mask;
    private VideoPlayer Video;
    private GameObject Closebutton;
    private AudioSource Audio;
    bool videoflag;

    public void Start()
    {
        videoflag = false;
        Video = this.GetComponent<VideoPlayer>();
        //Audio = this.GetComponent<AudioSource>();
    }
    public void PlayAD()
    {
        Closebutton = Resources.Load<GameObject>("Prefabs/ADclose");

        DisEn(false);

        StartCoroutine(playVideo());
        mask.enabled=false;
        rawimage.enabled = false;       
    }


    IEnumerator playVideo()
    {

        mask.transform.SetAsLastSibling();
        rawimage.transform.SetAsLastSibling();
        Video.Prepare();
        mask.enabled = false;
        WaitForSeconds wait = new WaitForSeconds(0.5f);

        while (!Video.isPrepared)
        {
            
            yield return wait;
            break;
        }
        mask.enabled = true;
        rawimage.enabled = true;
        Closebutton = Instantiate(Closebutton, Closebutton.transform.position, Closebutton.transform.rotation);
        Closebutton.transform.SetParent(transform.parent, false);

        //scaling
        /*
         float = scale;
        if (Video.texture.width / rawimage.rectTransform.rect.width > Video.texture.height / rawimage.rectTransform.rect.width)
        {
            scale = Video.texture.height * rawimage.rectTransform.rect.width/ Video.texture.width;
            Debug.Log(scale);
            Debug.Log(rawimage.rectTransform.rect.width);
            rawimage.rectTransform.rect.Set
            rawimage.uvRect = new Rect(rawimage.transform.position.x, rawimage.transform.position.y, rawimage.rectTransform.rect.width, scale);
        }
        else
        {
            scale = Video.texture.height * rawimage.rectTransform.rect.height * Video.texture.height/ Video.texture.width;
            rawimage.uvRect = new Rect(rawimage.transform.position.x, rawimage.transform.position.y, scale, rawimage.rectTransform.rect.height);
        }
        */

        rawimage.texture = Video.texture;
        Video.Play();
        videoflag = true;
        //Audio.Play();
    }
    
    private void FixedUpdate()
    {
        if (videoflag == true)
        {
            if (Video.isPlaying == false)
            {
                videoflag = false;
                mask.enabled = false;
                rawimage.enabled = false;

                PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 500);
                if (Closebutton!=null) Destroy(Closebutton);
                DisEn(true);
            }
        }
    }

    private void DisEn(bool b)
    {
        Transform par = transform.parent;
        int childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<Button>() != null) par.GetChild(i).gameObject.GetComponent<Button>().enabled = b;
        }

        par = transform.parent.gameObject.transform.parent;
        childCnt = par.childCount;

        for (int i = 0; i < childCnt; i++)
        {
            if (par.GetChild(i).gameObject.GetComponent<FrameController>() != null) par.GetChild(i).gameObject.GetComponent<FrameController>().enabled = b;
            if (par.GetChild(i).gameObject.GetComponent<LetterController>() != null) par.GetChild(i).gameObject.GetComponent<LetterController>().enabled = b;
        }

    }

}

