﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.UI;
using System;


namespace Tests
{
    public class GameTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void GameTestSimplePasses()
        {
            Assert.Pass();
            // Use the Assert class to test conditions           
            //if (scorebeforehand >= PlayerPrefs.GetInt("coins")) Assert.Fail();
        }


        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator GameTestWithEnumeratorPasses()
        {

            EditorSceneManager.OpenScene("Assets/Scenes/Game.unity");
            Transform par = UnityEngine.Object.FindObjectOfType<Canvas>().transform.parent.transform;

            int scorebeforehand = PlayerPrefs.GetInt("coins");
            for (int i = 0; i < par.childCount; i++)
            {
                if (par.GetChild(i).GetComponent<FrameController>() != null) par.GetChild(i).GetComponent<FrameController>().setState(true);
            }
            if (scorebeforehand >= PlayerPrefs.GetInt("coins")) Assert.Fail();
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
